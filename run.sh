# need npm install reveal-md
PATH="$PATH:$(pwd)/node_modules/.bin/"
reveal-md slides.md $* --theme simple --css fifix.css --highlight-theme github --template reveal.html
